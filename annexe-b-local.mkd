---
title: Règlement du local
subtitle: Annexe B au règlement interne
date: 2024-12-17
---

Ce règlement du local a pour but de définir les règles d'utilisation
du local CM 0 415 partagé par gnugen et Artepoly.


## Responsables Local

Chaque commission possède un·e Responsable Local qui coopèrent. Les Responsables 
Local sont des membres du comité dont la responsabilité est la bonne tenue du local
telle que définie dans ce document.


## Responsabilité individuelle

Chaque personne faisant usage du local est responsable de sa bonne tenue. Les membres
des deux commissions doivent en particulier prendre soin du matériel et le ranger
après utilisation, afin d'éviter au maximum de salir le local.


## Nettoyage

Chaque personne usant du local a la responsabilité de maintenir la propreté du local,
et se doit en particulier de nettoyer le matériel utilisé ou les éventuelles surfaces
salies.

Les Responsables Local doivent organiser un nettoyage complet du local au moins
une fois par semestre avec l'aide des personnes usant du local.


## Dommages au matériel

Les Responsables Local des deux commissions doivent immédiatement être prévenu·e·s en cas
de dégats au matériel commun aux deux commissions. En cas de dommage sur du matériel
d'une commission, au moins le·la Responsable de la commission en question doit être mi·se
au courant. Le comité détermine au cas par cas la participation financière, si nécessaire,
à la réparation des dégâts due par les personnes usant du local responsables de ceux-ci.


## Appareils électroniques du local

Tout appareil électronique faisant partie du matériel du local doit être éteint
s'il n'est pas utilisé. En particulier, aucun appareil électronique inutilisé ne
doit être laissé allumé toute la nuit. Ceci n'est pas le cas du téléphone fixe
du local et du NAS de gnugen.


## Dépôt de matériel

Les personnes faisant usage du local sont autorisées à déposer leur matériel au local
sous les conditions suivantes:

- Chaque objet déposé doit porter un identifiant contactable et une date de
  dépôt. Un identifiant contactable est une manière de contacter le·la
  propriétaire de l'objet, et peut consister en une adresse E-mail, un
  identifiant Matrix ou un numéro de téléphone ainsi que le nom de la commission
  dont il·elle est membre.
  - Si la date de dépôt est dépassée d'au moins 7 jours, tout·e membre a la
    responsabilité d'informer les Responsables Local, qui utilisent l'identifiant
    pour contacter le·la propriétaire de l'objet. Si l'objet est toujours
    présent 7 jours après la tentative de contact, il est considéré comme
    abandonné.
- Un objet qui porte une date de dépôt dans le futur est considéré comme
  abandonné.
- Si un objet déposé ne porte pas un identifiant et une date de dépôt, chaque
  membre a la responsabilité d'indiquer sur l'objet qu'il sera considéré comme
  abandonné à partir de 7 jours après la date de découverte de l'objet.
- Toute personne faisant usage du local peut soit s'approprier un objet abandonné,
  soit informer les Responsables Local, qui ont la responsabilité de s'en débarrasser,
  que ce soit en le proposant à des membres des commissions intéressé·e·s ou à des tiers,
  ou en le jetant.
  - Les objets de valeur sont amenés au service des objets trouvés de l'EPFL.
- Tout objet toxique ou posant un danger immédiat aux membres (e.g., une pile qui
  fuit) doit immédiatement être débarrassé par la personne usant du local qui l'a découvert,
  sans attendre le délai habituel de 7 jours pour considérer l'objet abandonné.


## Dépôt de nourriture

Le dépôt de nourriture ou de boissons conservées de façon appropriée (suivant les 
instructions de conservation sur l'emballage, et avant la date de péremption) à court
terme et à des fin d'événements d'une des 2 commissions partageant le local est autorisé. 
Les éléments doivent être datés et déposé dans les armoires. Tout dépôt extraordinaire de 
nourriture en dehors des armoires ne doit pas durer plus de 7 jours et être étiqueté
comme le matériel défini ci-dessus.


## Déchets

Le local ne possède pas de poubelles, et aucun dépôt de déchets n'y est toléré.
Les personnes usant du local sont priées d'utiliser les poubelles mises à disposition par
l'EPFL, par exemple celles se trouvant directement à l'extérieur du local.


## Accès Camipro de la porte

Chaque commission décide des membres ayant accès au local. Chaque commission a
la responsabilité de retirer les accés camipro des membres quittant la commission.


## Autres commissions

Les autres commissions faisant usage du local de gnugen et Artepoly se doivent
d'observer les règles définies dans ce document. Les Responsables Local sont
responsables de la communication avec ces autres commissions.
 

## Modification de ce document

Ce document peut être modifié à tout moment par un vote à majorité absolue des 2
comités. Chaque modification doit être annoncée aux membres des 2 commissions, et
entre en vigueur 7 jours après l'annonce.

