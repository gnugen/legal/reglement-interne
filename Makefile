.SUFFIXES:
.SUFFIXES: .mkd .html .pdf
.PHONY: all clean

SOURCES ::= $(wildcard *.mkd)
PDFS ::= $(SOURCES:%.mkd=%.pdf)
HTMLS ::= $(SOURCES:%.mkd=%.html)

.mkd.html .mkd.pdf:
	pandoc --standalone --shift-heading-level-by=-1 --number-sections -o $@ $<

all: $(PDFS) $(HTMLS)

clean:
	-rm -f *.pdf *.html
