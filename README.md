# Règlement interne de gnugen

L'AGEPoly a demandé à toutes les commissions de créer ou d'officialiser un
document les décrivant pour la rentrée d'automne 2019.

D’après le règlement des CdA:

1. Chaque CdA doit  écrire un règlement interne qui a pour but de présenter
   le fonctionnement général de la CdA. Il comprend les points suivants :

	1. Description de la CdA, de ses buts et de ses activités ;
	1. Notion de membre de CdA (si applicable), qui peut différer de la
	   notion de membre AGEPoly telle que définie dans les statuts ;
	1. Postes du comité ;
	1. Processus d’élection des comités ;
	1. Date de début et de fin de mandat ;
	1. Dispositions relatives aux sous-commissions le cas échéant (y compris
	   les accréditations).

1. Ce règlement doit être envoyé au CdD au moment de la demande de  validation
   du Président de la CdA et à chaque modification.

1. En cas de conflit ou de manque de précisions, les statuts de l’AGEPoly  et
   leurs règlements priment sur le règlement interne de la CdA.
